-------------------------------------------------------------------------------------
#Syzygium Version 1.0.0 
####12-Jan-2016 Sachin Gaikwad gaikwad411@gmail.com, 
####s.gaikwad@splendornet.com
-------------------------------------------------------------------------------------
Boilerplate for javascript applications developed using angular js.



-------------------------------------------------------------------------------------
##General Usage Notes
-------------------------------------------------------------------------------------
This application can be used for angular js applications being built
from scratch. Or if you find some/all components usefull you can copy and
use it as you want.
In genral case, you will need to clone the repository to your development
environment. Please follow the "Getting Started" notes to use it.


-------------------------------------------------------------------------------------
##Getting Started
-------------------------------------------------------------------------------------
1. Clone the repository https://sachin-snt@bitbucket.org/sachin-snt/syzygium.git
2. Go to the syzygium/app/src directory.
3. Execute following commands to pull all dependencies

```
   npm install
   bower install
```

4. Run following command to start the development

```
   gulp
```   
   
-------------------------------------------------------------------------------------
##Directory Structure
-------------------------------------------------------------------------------------
*syzygium

 *-- app
 
	*-- build --> All the files ready to deoploy including index html, css, js, images.
	
	*-- src   --> All the files where development will be carried out. 
			Build process takes files from "src" directory as input and 
			outputs deploy ready files in "build" directory.
			
	*-- tmp   --> Directory required by build process.
	
