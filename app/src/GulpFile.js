var appName = "syzygium"
// Required gulp plugins
var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-jsmin');
var replace = require('gulp-replace');
var watch = require('gulp-watch');
var cssnano = require('gulp-cssnano');
var del = require('del');
var templateCache = require('gulp-angular-templatecache');
var ngConstant = require('gulp-ng-constant');


// Build Dir Path
var buildDirPath = "../build/";
var tmpDirPath = "../tmp/";
var configDirPath = "../config/"




// Vendor JS Paths e.g. Angular, Jquery, Bootstrap
// Please add path of minified versions of JS here
var vendorJSPaths = [
	"bower_components/angular/angular.min.js", // angular 
	"bower_components/angular-ui-router/release/angular-ui-router.min.js", // angular router
	"bower_components/angular-resource/angular-resource.min.js",// angular resource
	"bower_components/angular-local-storage/dist/angular-local-storage.min.js", // local storage
	"bower_components/jquery/dist/jquery.min.js", // jquery
	
];
// 	APP JS
var appJSPaths = [
	"js/**/*.js", 
	"modules/**/js/**/*.js"
];

// APP Html
var appHTMLPaths = [
	"**/*.html",
	"!index.html",
	"!bower_components/**/*.html",
	"!node_modules/**/*.html",
];

///////////////////////////////////////////////////////////////////////////////////////
// JS Tasks for -> app js and vendor js
// 1. Minify JS (Avoid already vendor js 
//		- we need to add minified versions in as src)
// 2. Unify JS
// 3. Publish JS to build directory

///////////////////////////////////////////////////////////////////////////////////////
// Build html template cache
gulp.task("build-template-cache-tmp",function(){
	return gulp.src(appHTMLPaths)
    .pipe(templateCache("template.js",{
		//"root":"partials",
		"module": appName
		
	}))
    .pipe(gulp.dest(tmpDirPath+"js"));
});
// Build vendors tmp js 
gulp.task("buld-vendor-tmp-js",function(){
	// Build vendor js tmp file
	return gulp.src(vendorJSPaths)
	.pipe(concat("vendors-tmp.js"))
	.pipe(gulp.dest(tmpDirPath+"js"));
});
// Build app tmp js
gulp.task("build-app-tmp-js",function(){
	// Build app js tmp file
	return gulp.src(appJSPaths)
	.pipe(minify())
	.pipe(concat("app-tmp.js"))
	.pipe(gulp.dest(tmpDirPath+"js"));
});
// Build app tmp config js
gulp.task("build-app-config-tmp", function () {
  gulp.src(configDirPath+'app-config.json')
    .pipe(ngConstant({
      name: 'syz-config',
      deps: [],
      //wrap: 'amd',
    }))
    // Writes config.js to dist/ folder 
    .pipe(gulp.dest(tmpDirPath+"js"));
});

// Build App JS 
gulp.task("build-js",["build-app-tmp-js","buld-vendor-tmp-js",
	"build-template-cache-tmp","build-app-config-tmp"],function(){
	// Build app js by concating vendors tmp and 
	// app tmp js
 	return gulp.src([
		tmpDirPath+"js/vendors-tmp.js",
		tmpDirPath+"js/app-config.js",
		tmpDirPath+"js/app-tmp.js",
		tmpDirPath+"js/template.js"
	])
	.pipe(concat("app.js"))
	.pipe(gulp.dest(buildDirPath+"js"))
});
///////////////////////////////////////////////////////////////////////////////////////
// CSS build
// Vendor CSS Paths e.g. Bootstrap
// Please add path of minified versions of CSS here
var vendorCSSPaths = [
	
];
// 	APP CSS
var appCSSPaths = [
	"css/**/*.css",
	"modules/**/css/**/*.css"
];
///////////////////////////////////////////////////////////////////////////////////////
// CSS Tasks for app -> app css and vendor css
// 1. Minify CSS (Avoid vendor css 
//		- we need to add minified versions in as src)
// 2. Unify CSS
// 3. Publish CSS to build directory
// Build vendors tmp css 
gulp.task("buld-vendor-tmp-css",function(){
	// Build vendor css tmp file
	return gulp.src(vendorCSSPaths)
	.pipe(concat("vendors-tmp.css"))
	.pipe(gulp.dest(tmpDirPath+"css"));
});
// Build app tmp css
gulp.task("build-app-tmp-css",function(){
	// Build app css tmp file
	return gulp.src(appCSSPaths)
	.pipe(cssnano())
	.pipe(concat("app-tmp.css"))
	.pipe(gulp.dest(tmpDirPath+"css"));
});
// Build App CSS 
gulp.task("build-css",["build-app-tmp-css","buld-vendor-tmp-css"],function(){
	// Build app css by concating vendors tmp and 
	// app tmp css
 	return gulp.src([
		tmpDirPath+"css/vendors-tmp.css",
		tmpDirPath+"css/app-tmp.css"
	])
	.pipe(concat("app.css"))
	.pipe(gulp.dest(buildDirPath+"css"))
});
///////////////////////////////////////////////////////////////////////////////////////
// Images build
var appImagesPaths = [
	"images/**/*",
	"modules/**/images/**/*"
];
gulp.task("app-images-build",function(){
	return gulp.src(appImagesPaths)
	.pipe(gulp.dest(buildDirPath+"images"));
});
///////////////////////////////////////////////////////////////////////////////////////
// Build Site Index
gulp.task("build-index-html",function(){
	
	// App Version
	appVersion = "1.0.0."+Math.floor(Date.now() / 1000);
	
	// Add CSS include tags
	cssIncludeTags = "";
	cssIncludeTags += "<link rel=\"stylesheet\" href=\"css/app.css?version="+appVersion+"\"/>";
	
	// Add JS include tags
	jsIncludeTags = "";
	jsIncludeTags += "<script src=\"js/app.js?version="+appVersion+"\"></script>";
	
	return gulp.src("index.html")
	.pipe(replace("<!-- SYZ:GULP-APP-JS -->","<!-- SYZ:APP-JS -->"+jsIncludeTags+"<!-- /SYZ:APP-JS -->"))
	.pipe(replace("<!-- SYZ:GULP-APP-CSS -->","<!-- SYZ:APP-CSS -->"+cssIncludeTags+"<!-- /SYZ:APP-CSS -->"))
	.pipe(gulp.dest(buildDirPath));
})


///////////////////////////////////////////////////////////////////////////////////////
// Clean Build and Tmp Files
gulp.task("clean",function(cb){
	return del([tmpDirPath, buildDirPath],{force:true},cb);
});
///////////////////////////////////////////////////////////////////////////////////////
// Build task
gulp.task("build",["clean"],function(){
	
	gulp.start("build-js","build-css","app-images-build","build-index-html");
	
	// Add Config File Watcher
	gulp.watch(configDirPath+"app-config.json", 
		["build-js","build-index-html"]);

	// Add JS Watcher
	 gulp.watch(appJSPaths, 
		["build-js","build-index-html"]);
	 
	 // Add HTML Watcher
	 gulp.watch(appHTMLPaths, 
		["build-js","build-index-html"]);

	 gulp.watch("index.html", 
		["build-index-html"]);
	 
	// Add CSS Watcher
	 gulp.watch(appCSSPaths, 
		["build-css","build-index-html"]);
	 
	 // Add Images Watcher
	 return gulp.watch(appImagesPaths, ["app-images-build"]);
	 
});

// Default task
gulp.task("default", ["build"], function() {
	// Other tasks to run as default
});
