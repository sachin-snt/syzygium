// Declare App
var appName = appName || "syzygium";
var app = app || angular.module(appName, ["ui.router","syz-config", "ngResource", "LocalStorageModule"]);

// Local storage save config
app.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('syzygium')
    .setStorageType('localStorage')
    .setNotify(true, true);
});

// Http intercepter module
app.config(["appConfig","$httpProvider", function(appConfig, $httpProvider){
    var defaultHeaders = {
        "Content-Type": "application/json",
        "Accept-Language": appConfig.defaultLanguage || "en",
	//"Authorization":SyzAppSession.getToken()
    };

    $httpProvider.defaults.headers.delete = defaultHeaders;
    $httpProvider.defaults.headers.patch = defaultHeaders;
    $httpProvider.defaults.headers.post = defaultHeaders;
    $httpProvider.defaults.headers.put = defaultHeaders;
    $httpProvider.defaults.headers.get = defaultHeaders;

}]);

// Resource provider config
app.config(['$resourceProvider', function($resourceProvider) {
  // Don't strip trailing slashes from calculated URLs
  $resourceProvider.defaults.stripTrailingSlashes = false;
}]);

// Routes
app.config(function($stateProvider, $urlRouterProvider) {

  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/home");
  
  $stateProvider
    .state('home', {
      url: "/home",
      templateUrl: "partials/home.html"
    })
    .state('user-dashboard', {
      url: "/user-dashboard",
      templateUrl: "partials/user-dashboard.html",
      access:{requireLogin:true}	
    })
    .state('login', {
      url: "/login",
      templateUrl: "partials/login.html"
    })
    .state('about-us', {
      url: "/about-us",
      templateUrl: "partials/about-us.html"
    });
	
});

app.run(["$rootScope","SyzAppSession", "$state", function($rootScope, SyzAppSession, $state){

	$rootScope.$on("$stateChangeStart",function(e, toState  , toParams
                                                   , fromState, fromParams){

		if(toState.access && toState.access.requireLogin === true)
		{
			if(SyzAppSession.isAuthenticated()===false)
			{
			   e.preventDefault(); // stop current execution
			   $state.go("login");
			}
		                
		}
	});

}]);
// App session factory
app.factory("SyzAppSession",["$stateParams","$location","localStorageService", 
function($stateParams, $location, localStorageService){
     var session = {};
     session.isAuthenticated = false;
     session.token = "";
     session.user = {};
       
     // Set session
     var setSession = function(token, user){
	session.isAuthenticated = true;
        session.token;
	localStorageService.set("token", session.token);
	localStorageService.set("isAuthenticated", session.isAuthenticated);
     };

     //	Get token
     var getToken = function(){
	session.token = localStorageService.get("token");
	session.isAuthenticated = localStorageService.get("isAuthenticated");
	return session.token;
     };	

    // Var Check if user is authenticated
    var isAuthenticated = function(){
	session.token = localStorageService.get("token");
	session.isAuthenticated = localStorageService.get("isAuthenticated");
	return session.isAuthenticated;
    };

    // Var redirect to dashboard if authenticated user
    var redirectToDashBIfAuthenticated = function(){
	var redirectTo = "user-dashboard";
	if( $location.search()["redirectTo"]!==undefined 
		&& typeof $location.search()["redirectTo"] === String)
	{
		redirectTo = $location.search()["redirectTo"];
	}
	if(this.isAuthenticated()===true )
	{
		$location.path(redirectTo);
	}
    };		
			
     return {
		"setSession":setSession,
		"getToken":getToken,
		"isAuthenticated":isAuthenticated,
                "redirectToDashBIfAuthenticated":redirectToDashBIfAuthenticated
	};	
}]);


// App Version Directive
app.directive('syzAppVersion',function(){
	return  {
		template:"<span>Version 1.0.0 &copy; 2016, Sachin Gaikwad</span>",
		link:function($scope, $elm, $attrs){
			 
		}
	};
});

app.directive('syzAppLoadingBg',function(){
	return  {
		link:function($scope, $elm, $attrs){
			$elm.removeClass("app-loading-bg");
		}
	};
});

app.controller("homeController",function($scope, syzResAuth){
 	$scope.userRegisterData = {form:{}};
        $scope.registerUser = function(){
		
		if($scope.userRegisterData.form.$valid !== true)
		{
			alert("Form is invalid.");	
                        return false;   		
		}
		syzResAuth.registerUser({
			username: angular.copy($scope.userRegisterData.userName),
			password: angular.copy($scope.userRegisterData.password)
		}).then(function(resp){
			alert("User registered successfully!!!");
		},function(resp){
			// On error
			alert("Server error for user registration.");	
		});
		return false;
        }; 
});
app.controller("loginController",function($scope, syzResAuth, SyzAppSession){
	SyzAppSession.redirectToDashBIfAuthenticated();

 	$scope.userLoginData = {form:{}};
        $scope.loginUser = function(){
		
		if($scope.userLoginData.form.$valid !== true)
		{
			alert("Form is invalid.");	
                        return false;   		
		}
		syzResAuth.loginUser({
			username: angular.copy($scope.userLoginData.userName),
			password: angular.copy($scope.userLoginData.password)
		}).then(function(resp){
			alert("User logged in successfully!!!");
		},function(resp){
			// On error
			alert("Server error for user login.");	
		});
		return false;
        }; 
});

