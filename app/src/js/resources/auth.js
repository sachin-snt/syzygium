var appName = appName || "syzygium"
var app = app || angular.module(appName, ["ui.router","syz-config", "ngResource","LocalStorageModule"])
app.factory("syzResAuth",function(appConfig, $resource, SyzAppSession){
    var AuthResource = $resource(appConfig.apiBase+"/auth/:action/",{},{
		"register":{method:"POST",params:{"action":"register"}},
		"login":{method:"POST",params:{"action":"login"}}
	});
    
    // Function to accept new user for registration	
    AuthResource.registerUser = function(newAuthUserData){
      newAuthUser = new AuthResource(newAuthUserData);
      return newAuthUser.$register();
    };

    // Function to login user
   AuthResource.loginUser = function(newAuthUserData){
      newAuthUser = new AuthResource(newAuthUserData);
      return newAuthUser.$login().then(function(result){
		
		if(result["auth_token"] !== undefined)
		{
		  SyzAppSession.setSession(result["auth_token"],{});
		}
		SyzAppSession.redirectToDashBIfAuthenticated();

	});
    };

    return AuthResource;	
});
